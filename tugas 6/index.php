<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new animal("Shaun");

echo "name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold_blooded : " . $sheep->cold_blooded . "<br><br>";

$frog = new frog("buduk");
echo "name : " . $frog->name . "<br>";
echo "legs : " . $frog->legs . "<br>";
echo "cold_blooded : " . $frog->cold_blooded . "<br>";
echo $frog->jump();


$ape = new ape ("kera sakti");
echo "name : " . $ape->name . "<br>";
echo "legs : " . $ape->legs . "<br>";
echo "cold_blooded : " . $ape->cold_blooded . "<br>";
echo $ape->yell();
?>