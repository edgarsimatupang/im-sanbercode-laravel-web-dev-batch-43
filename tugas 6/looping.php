<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Contoh Soal Looping PHP</h1>
    <?php
    echo "<h2>Contoh Soal 1 </h2>";

    echo "<h4>looping 1</h4>";
    for($i = 2; $i<=20; $i+=2){
        echo $i . "
         - I Love PHP <br>";
    }

    echo "<h4>Looping 2</h4>";
    $x = 20;
    do {
        echo "$x - I love PHP <br>";;
        $x-=2;
    } while ($x >= 0);

    echo "<h3>Contoh Soal 2 </h3>";

    $nomor = [18, 45, 29, 61, 47, 34];
    echo "Array nomor : " ;
    print_r($nomor);
    echo "<br>";
    foreach($nomor as $value){
        $tampung[] = $value%5;
        
    }

    echo "Array sisa bagi 5 adalah: ";
    print_r($tampung);

    

    echo "<h4>Looping 3</h4>";
    $item = [
        ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
        ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpeg"],
        ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
        ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpeg"],

    ];
     
    foreach($item as $arrayindex){
        $items = [
            'id' => $arrayindex[0],
            'name' => $arrayindex[1],
            'price' => $arrayindex[2],
            'description' => $arrayindex[3],
            'source' => $arrayindex[4],

        ];
        print_r($items);
        echo "<br>";
    }

    echo "<h4>Looping 4</h4>";

    for($k=1; $k<=5; $k++){
       for($l=$k; $l<=5; $l++){
        echo " * ";

       }
       echo "<br>";
    }


    ?>
</body>
</html>