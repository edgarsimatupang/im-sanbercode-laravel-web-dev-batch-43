@extends('layouts.master')
@section('title')
  Halaman Biodata
@endsection

@section('sub-title')
    Biodata
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
   <h2>Sign Up Form</h2>

   <form action="/welcome" method="post">
    @csrf
    <label>First name</label><br>
    <input type="text" name="fname"> <br><br>
    <label>Last name</label><br>
    <input type="text" name="lname"> <br><br>
    <label>Gender:</label><br>
    <input type="radio" name="status" >Male<br>
    <input type="radio" name="status" >Female<br>
    <input type="radio" name="status" >Other<br><br>
    <label>Nationality:</label><br><br>
    <select name="status" style="width:100px; height: 30px" readonly>
        <option value = "Indonesian">Indonesian</option>
        <option value = "Malaysia">Malaysia</option>
        <option value = "Thailand">Thailand</option>
    </select><br><br>
    <label>Language Spoken:</label><br>
    <input type="checkbox" name="Language Spoken">Bahasa Indonesia<br>
    <input type="checkbox" name="Language Spoken">English<br>
    <input type="checkbox" name="Language Spoken">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea cols="40" rows="10"></textarea><br><br>

    <input  type="submit" value="Sign Up">


   </form>


@endsection
