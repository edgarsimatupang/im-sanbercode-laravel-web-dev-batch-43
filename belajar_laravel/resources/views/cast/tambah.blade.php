@extends('layouts.master')
@section('judul')
  Halaman Tambah Cast
@endsection

@section('sub-title')
Halaman Cast
@endsection

@section('content')
<form>
  <div class="form-group">
    <label>Nama Cast/label>
    <input type="text" name = "name" class="form-control" cols="30" rows="10"></textarea>
  </div>
  <div class="form-group">
    <label>Biography Cast</label>
    <textarea name="bio" class="form-control" ></input>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
