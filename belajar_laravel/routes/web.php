<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Htpp\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);


Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome',  [AuthController::class, 'welcome']);

//CRUD CAST
//Create Data
//router untuk mengarah ke halaman genre
Route::get('/cast/create', [CastController::class, 'create']);
